package de.edu.uniBonn.BigData.RabinFingerPrint;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

import de.edu.uniBonn.BigData.RabinFingerPrint.DataModel.Document;
import de.edu.uniBonn.BigData.RabinFingerPrint.FiniteFields.Polynomial;
import de.edu.uniBonn.BigData.RabinFingerPrint.Util.RandomString;
import de.edu.uniBonn.BigData.RabinFingerPrint.matrixBuilder.MatrixUtiliy;

public class Algorithm {

	public static void main(String []args)
	{
		Document[] documents =new Document[100];
		/*
		List<Polynomial> testPolynomials=new ArrayList<Polynomial>();
		Polynomial p1=new Polynomial(getTree(new String[]{"0","1","4","7","9","11","12","13","14","15","16"}));
		Polynomial p2=new Polynomial(getTree(new String[]{"0","1","4","8","14","6","16"}));
		Polynomial p3=new Polynomial(getTree(new String[]{"0","1","4","7","9","11","12","13","14","15","17","18"}));

		testPolynomials.add(p1); testPolynomials.add(p2);testPolynomials.add(p3);
		Polynomial p4=new Polynomial(getTree(new String[]{"0","1","4","7","9","11","12","13","14","15","16"}));
		System.out.println(testPolynomials.contains(p3));
		for(int i=0;i<testPolynomials.size();i++)
		{
			Polynomial tmp=testPolynomials.get(i);
			System.out.println(tmp.compareTo(p4));
		}
		*/
		
		
		// create d1
		RandomString randomString=new RandomString(1000);
		String d1=randomString.nextString();
		//System.out.println(d1);
		Random rand = new Random();
		documents[0]=(new Document(d1));
		int k=5; // number of random character replacement
		int l=7; // number of swapping characters 
		for(int i=1;i<100;i++)
		{
			String tmpString=d1;
			// adding noise
			// replace k character
			for(int j=0;j<k;j++)
			{
				char[] charArray = tmpString.toCharArray();
				charArray[rand.nextInt(tmpString.length())]=randomString.getRandomCharacter();
				tmpString=new String(charArray);	
			}
			// swap l times
			for(int j=0;j<l;j++)
			{
				char[] charArray = tmpString.toCharArray();
				int pos1=rand.nextInt(tmpString.length());
				int pos2=rand.nextInt(tmpString.length());
				char tmp=charArray[pos1];
				charArray[pos1]=charArray[pos2];
				charArray[pos2]=tmp;
				tmpString=new String(charArray);
			}
			// adding more noise for next iteration
			k+=5; 
			l+=7;
			documents[i]=(new Document(tmpString));
		}
		System.out.println("done with generating 100 files!!!");
		//System.out.println(documents[0].getContent());
		
		
		
		// our irreducable polynmial is x^16+x^15+x^14+x^13+x^12+x^11+x^9+x^7+x^4+x+1
		Polynomial irreducablePolynomial=new Polynomial(getTree(new String[]{"0","1","4","7","9","11","12","13","14","15","16"}));
		
		//Polynomial irreducablePolynomial=new Polynomial(getTree(new String[]{"16","15","14","13","12","11","9","7","4","1","0"}));

		
		//irreducablePolynomial.setDegree(new BigInteger("16"));
		
		System.out.println("the irreducable polynomial is "+irreducablePolynomial.toPolynomialString());
		//System.out.println("degree="+irreducablePolynomial.degree());
		int count=0; // count how many we compute the jaccard distance between d1 and others
		int qValue=5; // initial value for q 
		MatrixUtiliy matrixBuilder=new MatrixUtiliy();
		for(count=0;count<10;count++)
		{
			matrixBuilder.buildMatrix(documents, qValue,irreducablePolynomial);
			
			// compute the Jaccard similarity between D1 and others 
			String MSimilarity="";
			String approximatedSimilarity="";
			for(int i=1;i<100;i++)
			{
				MSimilarity=MSimilarity+" "+matrixBuilder.computeJacardDistanceInM(i)+",";
				approximatedSimilarity=approximatedSimilarity+" "+matrixBuilder.computeJacardDistanceInMs(i)+",";
				
			}
			System.out.println("with q="+qValue+",similarity between D1 and other documents in M are");
			System.out.println(MSimilarity);
			
			System.out.println("with q="+qValue+",similarity between D1 and other documents in Ms are");
			System.out.println(approximatedSimilarity);
			System.out.println("################################################################################");
			
			qValue+=20; // adding more shingle
		}
		
	}
	
	
	private static final class ReverseComparator implements Comparator<BigInteger> {
		public int compare(BigInteger o1, BigInteger o2) {
			return -1 * o1.compareTo(o2);
		}
	}
	
	
	private static TreeSet<BigInteger> getTree(String[] args)
	{
		TreeSet<BigInteger> result=new TreeSet<BigInteger>(new ReverseComparator());
		for(String tmp:args)
		{
			result.add(new BigInteger(tmp));
			
		}
		return result;
	}
}
