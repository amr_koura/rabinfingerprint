package de.edu.uniBonn.BigData.RabinFingerPrint.DataModel;

import java.util.ArrayList;
import java.util.List;

import de.edu.uniBonn.BigData.RabinFingerPrint.FiniteFields.Polynomial;

public class Document {

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	private String content;
	

	public List<String> getShingles() {
		return shingles;
	}

	private List<String>shingles;
	public void setShingles(List<String> shingles) {
		this.shingles = shingles;
	}

	public List<Polynomial> getFingerPrints() {
		return fingerPrints;
	}

	public void setFingerPrints(List<Polynomial> fingerPrints) {
		this.fingerPrints = fingerPrints;
	}

	private List<Polynomial> fingerPrints;
	
	public Document(String content)
	{
		this.content=content;
		
	}
	/**
	 * get number of shingles
	 * @return
	 */
	public int getNumberOfShingles()
	{
		return shingles.size();
	}
	
	/**
	 * compute the set of fingerPrints to related documents
	 * @param irrducablePolynomail
	 */
	public void buildFingerPrint(Polynomial irrducablePolynomail)
	{
		fingerPrints=new ArrayList<Polynomial>();
		for(int i=0;i<shingles.size();i++)
		{
			Polynomial tmp=Polynomial.createFromBytes(shingles.get(i).getBytes());
			//System.out.println(tmp.toPolynomialString());
			
			tmp=tmp.mod(irrducablePolynomail);
			//System.out.println(tmp.toPolynomialString());
			fingerPrints.add(tmp);
			
		}
	}
	
}
