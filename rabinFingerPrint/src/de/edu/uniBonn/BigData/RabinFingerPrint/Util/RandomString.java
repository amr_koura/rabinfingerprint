package de.edu.uniBonn.BigData.RabinFingerPrint.Util;

import java.util.Random;

public class RandomString {

	

	
	  private final Random random = new Random();
	  
	  private static final int ASCII_CHARS = 256;

	  private final char[] buf;

	  public RandomString(int length) {
	    if (length < 1)
	      throw new IllegalArgumentException("length < 1: " + length);
	    buf = new char[length];
	  }

	  public String nextString() {
	    for (int idx = 0; idx < buf.length; ++idx) 
	      buf[idx] = (char) random.nextInt(ASCII_CHARS);
	    return new String(buf);
	  }
	  
	  public char getRandomCharacter()
	  {
		  //return symbols[random.nextInt(symbols.length)];
		  int randomInt=random.nextInt(256); // from 0 to 255
		  return Character.toChars(randomInt)[0];
		  
	  }
	  
}
