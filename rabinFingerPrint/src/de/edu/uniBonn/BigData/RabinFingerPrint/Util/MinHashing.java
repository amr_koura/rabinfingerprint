package de.edu.uniBonn.BigData.RabinFingerPrint.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MinHashing {

	List<Integer> permulation;
	
	
	public MinHashing(int r) {
		// TODO Auto-generated constructor stub
		permulation=new ArrayList<Integer>();
		permulation=getRandomPermutation(r);
	}
	
	/**
	 * apply min hash function
	 * @param M
	 * @return
	 */
	public int[] hash(byte[][]M)
	{
		// take random permutation of [r],r:# of rows of M
		int [] result=new int[M[0].length]; // number of colomns in the the hash value equals to number of colomns in M
		for(int i=0;i<result.length;i++)
		{
			
			for(int k=0;k<M.length;k++)
			{
				int index=permulation.indexOf(k+1);
				if(M[index][i]==1)
				{
					result[i]=k;
					break;
					
				}
			}
		}
		
		
		return result;
		
	}
	
	/**
	 * get random permutation of [r]
	 * @param r
	 * @return
	 */
	private List<Integer> getRandomPermutation(int r) {
		Random rnd = new Random();
				List <Integer> permutation = new ArrayList<Integer>();
				int count = 0;
				while (count < r) {
					int curRndNum = rnd.nextInt(r)+1;
					if (!permutation.contains(curRndNum)) {
						++count;
						permutation.add(curRndNum);
					}
				}
				return permutation;
	}
	
	
	/*
	public List<List<Integer>> permute(int[] numbers) {
	   
	    List<List<Integer>> permutations = new ArrayList<List<Integer>>();
	    permutations.add(new ArrayList<Integer>());

	    for ( int i = 0; i < numbers.length; i++ ) {
	       
	        List<List<Integer>> current = new ArrayList<List<Integer>>();
	        for ( List<Integer> permutation : permutations ) {
	            for ( int j = 0, n = permutation.size() + 1; j < n; j++ ) {
	                List<Integer> temp = new ArrayList<Integer>(permutation);
	                temp.add(j, numbers[i]);
	                current.add(temp);
	            }
	        }
	        permutations = new ArrayList<List<Integer>>(current);
	    }

	    return permutations;
	}
	*/
	
	
}
