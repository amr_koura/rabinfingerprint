package de.edu.uniBonn.BigData.RabinFingerPrint.FiniteFields;

public class Polynomials {
	

	/**
	 * Returns the value of the bit at index of the byte. The right most bit is
	 * at index 0.
	 */
	public static boolean getBit(byte b, int index) {
		return (((b >> index) & 1) == 1);
	}

	/**
	 * Returns the value of the bit at index of the byte. The right most bit is
	 * at index 0 of the last byte in the array.
	 */
	public static boolean getBit(byte[] bytes, int index) {
		// byte array index
		final int aidx = bytes.length - 1 - (index / 8);
		// bit index
		final int bidx = index % 8;
		// byte
		final byte b = bytes[aidx];
		// bit
		return getBit(b, bidx);
	}

}
