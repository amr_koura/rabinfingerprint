package de.edu.uniBonn.BigData.RabinFingerPrint.matrixBuilder;

import java.util.ArrayList;
import java.util.List;

import de.edu.uniBonn.BigData.RabinFingerPrint.DataModel.Document;
import de.edu.uniBonn.BigData.RabinFingerPrint.FiniteFields.Polynomial;
import de.edu.uniBonn.BigData.RabinFingerPrint.Util.MinHashing;

public class MatrixUtiliy {

	private byte [][]  M;
	
	
	public byte[][] getM() {
		return M;
	}

	

	public int[][] getMs() {
		return Ms;
	}



	private int [][] Ms;
	
	/**
	 * 
	 * @param documents
	 * @param q
	 * @param irreducablePolynomial
	 */
	public void buildMatrix(Document []documents,int q,Polynomial irreducablePolynomial)
	{
		// split the documents to shingles
		for(int i=0;i<documents.length;i++)
		{
			documents[i].setShingles(getShingles(documents[i].getContent(), q));
			documents[i].buildFingerPrint(irreducablePolynomial);
		}
		// build M
		M=buildM(documents);
		
		// build Ms
		Ms=buildMs(documents);
		
	}
	
	/**
	 * divide the document to set of q-shingles 
	 * @param content
	 * @param q
	 * @return
	 */
	private List<String> getShingles(String content,int q)
	{
		List<String> shingles=new ArrayList<String>();
		int position=0;
		while(content.length()>=(position+q))
		{
			shingles.add(content.substring(position, position+q));
			position+=q;
		}
		
		return shingles;
		
	}
	
	
	
	/**
	 * build the Ms matrix obtained from the MinHashing, represet the sketch for Matrix M
	 * @param documents
	 * @return
	 */
	private int[][] buildMs(Document []documents){
		int [][] Ms=new int[100][100]; // rows represent number of hashing function and colomn represensts the document number
		
		for(int i=0;i<Ms.length;i++)
		{
			MinHashing hashFunction=new MinHashing(this.M.length);
			int [] hashResult=hashFunction.hash(this.M);
			Ms[i]=hashResult;
			
		}
		
		return Ms;
	}
	
	
	/**
	 * this function return the M matrix which represents the similarty between d1 and other documents
	 * @param documents
	 * @return
	 */
	private byte [][] buildM(Document []documents)
	{
		byte [][] M=new byte[documents[0].getNumberOfShingles()][100]; // number of row is equals to number of shingles , and number of colomns equals to number of documents
		// M(i,j)=1 iff shingle i is exists in document j
		// the jacard similiraty should be computed between the colomns
		for(int i=0;i<M.length;i++)
			for(int j=0;j<M[0].length;j++)
			{
				//if(documents[j].getShingles().contains(documents[0].getShingles().get(i)))
				//System.out.println("comparing "+documents[0].getFingerPrints().get(i).toPolynomialString()+"  and "+documents[j].getFingerPrints().get(3).toPolynomialString());
				//if(documents[j].getFingerPrints().contains(documents[0].getFingerPrints().get(i)))
				if (isContain(documents[0].getFingerPrints().get(i), documents[j].getFingerPrints()))
				{
					M[i][j]=1;
				}
				else
				{
					M[i][j]=0;
				}
			}
		return M;
	}
	
	private boolean isContain(Polynomial onePolynomial,List<Polynomial> polynomials)
	{
		for(int i=0;i<polynomials.size();i++)
			if(polynomials.get(i).compareTo(onePolynomial)==0)
				return true;
		return false;
	}
	
	/**
	 * compute the jacard similarity between D1 and other Document
	 * @param documentNumber
	 * @return
	 */
	public double computeJacardDistanceInM(int documentNumber)
	{
		double total=0;
		double similar=0;
		for(int i=0;i<M.length;i++)
			if(!(M[i][0]==0 && M[i][documentNumber]==0))
			{
				total++;
				if(M[i][0]== M[i][documentNumber])
					similar++;
					
			}
		return similar/total;
		
	}
	
	/**
	 * compute the jacard similarity between D1 and other Document
	 * @param documentNumber
	 * @return
	 */
	public double computeJacardDistanceInMs(int documentNumber)
	{
		
		double similar=0;
		for(int i=0;i<Ms.length;i++)
			if(Ms[i][0]== Ms[i][documentNumber])
			{
				
				similar++;
					
			}
		return similar/Ms.length;
		
		
	}
	
}
